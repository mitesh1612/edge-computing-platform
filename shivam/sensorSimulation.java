import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.Random;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.*;
import org.json.simple.JSONObject;

class sensorSimulation {
	
	private static final String SEND_QUEUE = "lb_in_queue";
	private static  String SEND_QUEUE1 = "";
	private static ConnectionFactory Factory1,Factory;
	private static Connection send_connection, send_connection1;
	private static Channel send_channel, send_channel1;
	private static String ip = "";
	
	public JSONObject createData(int val) throws UnknownHostException
	{
		Random rand = new Random();
	  JSONObject obj = new JSONObject();
	  InetAddress ipAddr = InetAddress.getLocalHost();
      //obj.put("qname", "guest");
	  int x = rand.nextInt(6);
	  obj.put("qname", ip);
	  obj.put("app_id", "Patient_MON");
      obj.put("servicename", "logic");
      obj.put("priority", x+"");
      obj.put("type_param", "1");
      obj.put("param", Integer.toString(val));
      System.out.println("Priority is" + x);
      //obj.put("output format", "int");
      //obj.put("to", "localhost");
      return obj;
  
	}
	
	private static void initialize1() throws Exception{
		   
		   
		   Factory = new ConnectionFactory();
		   Factory.setUsername("guest");
		   Factory.setPassword("guest");
		   Factory.setVirtualHost("/");
		   Factory.setHost("10.3.0.136");
		   Factory.setPort(5672);
		   send_connection = Factory.newConnection();
		   send_channel = send_connection.createChannel();
		   send_channel.queueDeclare(SEND_QUEUE, false, false, false, null);
	}
	
	private static void initialize2() throws Exception{
		/*String interfaceName = "eth0";
	    NetworkInterface networkInterface = NetworkInterface.getByName(interfaceName);
	    Enumeration<InetAddress> inetAddress = networkInterface.getInetAddresses();
	    InetAddress currentAddress;
	    currentAddress = inetAddress.nextElement();
	    while(inetAddress.hasMoreElements())
	    {
	        currentAddress = inetAddress.nextElement();
	        if(currentAddress instanceof Inet4Address && !currentAddress.isLoopbackAddress())
	        {
	            ip = currentAddress.toString();
	            break;
	        }
	    }*/


		
//		InetAddress ipAddr = InetAddress.getLocalHost();
		SEND_QUEUE1 = ip;
//		System.out.println(SEND_QUEUE1);
	   Factory1 = new ConnectionFactory();
	   Factory1.setUsername("guest");
	   Factory1.setPassword("guest");
	   Factory1.setVirtualHost("/");
	   Factory1.setHost(ip);
	   Factory1.setPort(5672);
	   send_connection1 = Factory1.newConnection();
	   send_channel1 = send_connection1.createChannel();
	   send_channel1.queueDeclare(SEND_QUEUE1, true, false, false, null);
	}
	
	
	public static boolean readJSON(JSONObject newJObject) throws Exception
	{
		
		String qname = (String) newJObject.get("qname");
	 	 String app_id = (String) newJObject.get("app_id");
	 	 String priority = (String) newJObject.get("priority");
	 	 String servicename = (String) newJObject.get("servicename");
	 	 String param = (String) newJObject.get("param");
	 	 if(Integer.parseInt(priority) <= 2) 
	 	 {
	 		
	 		String output_string = null;
	 		
	 		String cmd = "python ";
	 	     cmd += servicename;
	 	     cmd += ".py ";
	 	     cmd += param;

	 		 	 Process p = Runtime.getRuntime().exec(cmd);
	 	     
	 		 	 BufferedReader buffReader = new BufferedReader(new InputStreamReader(p.getInputStream()));
	 	    
	 	    
	 	   while ((output_string = buffReader.readLine()) != null) {
	 		  send_channel1.basicPublish("",qname,MessageProperties.PERSISTENT_TEXT_PLAIN,output_string.getBytes("UTF-8"));
	        }
	 	   //send_channel1.basicPublish("", SEND_QUEUE1, null, retMsg.getBytes("UTF-8"));
	 		return true;
	 	 }
	 		 
	 	 return false;
	}
	
	
	public static String deviceName() throws Exception 
	{
		String device = "";
		try{
 
		String prg = "import subprocess as sb\nproc = sb.Popen([\"ifconfig -a | sed -n '1p'\"], stdout=sb.PIPE, shell=True)\n(out,err) = proc.communicate()\ndev = out.split()[0]\nprint dev\nproc = sb.Popen([\"ifconfig {} | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'\".format(dev)], stdout=sb.PIPE, shell=True)\n(out,err) = proc.communicate()\nprint out";
		BufferedWriter out = new BufferedWriter(new FileWriter("test1.py"));
		out.write(prg);
		out.close();

		 
		ProcessBuilder pb = new ProcessBuilder("python","test1.py");
		Process p = pb.start();
		 
		BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));

		device = in.readLine();

		//System.out.println("value is : "+in.readLine());
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		String prg = "import subprocess as sb\nproc = sb.Popen([\"ifconfig " +device+" | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'\"], stdout=sb.PIPE, shell=True)\n(out,err) = proc.communicate()\nprint out";
	BufferedWriter out = new BufferedWriter(new FileWriter("test1.py"));
	out.write(prg);
	out.close();

	 try {
	ProcessBuilder pb = new ProcessBuilder("python","test1.py");
	Process p = pb.start();
	 
	BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
	device = in.readLine();
	//System.out.println("value is : "+in.readLine());
	}
	catch(Exception e){
		System.out.println(e);
	}
	return device; 
}
	
	
	
	
   public static void main(String[] args) throws Exception{
	   ip = deviceName();
	   initialize1();
		initialize2();
	  boolean flag = true, flag1 = true;
	  sensorSimulation datagen = new sensorSimulation();
	  //String QUEUE_NAME = "hello";
	  Random rand = new Random();
	  for(int i = 0 ;i<=9; i++)
	  {
	  //while(true)
	  //
			  
		  int x = rand.nextInt(128);
		  JSONObject obj =  datagen.createData(x);  
		  if(!readJSON(obj)) {
			  
			  send_channel.basicPublish("", SEND_QUEUE, null, obj.toString().getBytes("UTF-8"));

			  
		  }
		  //datagen.reciever();
		  System.out.println("after");
		  //channel.basicPublish("", QUEUE_NAME, null, message.getBytes("UTF-8"));
	  }
	  
	  send_channel.close();
	  send_connection.close();
	  send_channel1.close();
	  send_connection1.close();
	  //System.out.print("terminated");
	 
   }
}