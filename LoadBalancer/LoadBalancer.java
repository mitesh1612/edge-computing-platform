import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.*;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.ParseException;
import org.json.simple.parser.JSONParser;

class LoadBalancer {
	private final static String INPUT_QUEUE = "lb_in_queue";
	private static final String TASK_QUEUE_NAME = "lb_out_queue";
	public static String message;
	public static Channel channel1;
	
	static void setup_mq_sender() throws IOException, TimeoutException {
		List<String> ips = null;
		InetAddress inetAddress = InetAddress.getLocalHost();
		ConnectionFactory connectionFactory = new ConnectionFactory();
		connectionFactory.setUsername("guest");
		connectionFactory.setPassword("guest");
		connectionFactory.setVirtualHost("/");
		connectionFactory.setHost(inetAddress.getHostAddress());
		connectionFactory.setPort(5672);
		Connection connection = connectionFactory.newConnection();
		channel1 = connection.createChannel();
		channel1.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);
	}
	
   static void reciever() throws IOException, TimeoutException {
	   InetAddress inetAddress = InetAddress.getLocalHost();
	   ConnectionFactory connectionFactory = new ConnectionFactory();
	   connectionFactory.setUsername("guest");
	   connectionFactory.setPassword("guest");
	   connectionFactory.setVirtualHost("/");
	   connectionFactory.setHost(inetAddress.getHostAddress());
	   connectionFactory.setPort(5672);
	   Connection connection = connectionFactory.newConnection();
	   Channel channel = connection.createChannel();
	   
	   channel.queueDeclare(INPUT_QUEUE, false, false, false, null);
	   System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
	   Consumer consumer = new DefaultConsumer(channel) {
		   @Override
		   public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) 
				   throws IOException {
			   message = new String(body, "UTF-8");
			   channel1.basicPublish("", TASK_QUEUE_NAME,
				        MessageProperties.PERSISTENT_TEXT_PLAIN,
				        message.getBytes("UTF-8"));
			   System.out.println(" [x] Received '" + message + "'");
		   }
		 };
		 channel.basicConsume(INPUT_QUEUE, true, consumer);
		 
   }
	
   public static void main(String[] args) throws IOException, TimeoutException, ParseException{
	   setup_mq_sender();
	   reciever();
   }
}
