
public class ServiceInfo {
	private String name;
	private String fileLoc;
	private int noParams;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFileLoc() {
		return fileLoc;
	}
	public void setFileLoc(String fileLoc) {
		this.fileLoc = fileLoc;
	}
	public int getNoParams() {
		return noParams;
	}
	public void setNoParams(int noParams) {
		this.noParams = noParams;
	}
	@Override
	public String toString() {
		return "Service Name: "+this.name+"\nService File Location: "+this.fileLoc+"\nNumber of Parameters: "+this.noParams;
	}
}
