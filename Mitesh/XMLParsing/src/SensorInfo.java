
public class SensorInfo {
	private String name;
	private String id;
	private String ipAddress;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	@Override
	public String toString() {
		return "Sensor Name: "+this.name+"\nIP Address: "+this.ipAddress+"\nID: "+this.id;
	}
}
