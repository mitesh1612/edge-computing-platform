import java.io.File;
import java.io.IOException; 
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class SensorConfigReader {
	public static void main(String[] args) {
		String filePath = "sensors.xml";
		File xmlFile = new File(filePath);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);
			doc.getDocumentElement().normalize();
			System.out.println("Root Element: "+doc.getDocumentElement().getNodeName());
			NodeList nodeList = doc.getElementsByTagName("Sensor");
			// This loads the XML into the Memory
			List<SensorInfo> sensorList = new ArrayList<SensorInfo>();
			System.out.println("Number of Nodes in XML File: "+nodeList.getLength());
			for (int i=0;i<nodeList.getLength();i++) {
							sensorList.add(getSensor(nodeList.item(i)));
				}
			for(SensorInfo s : sensorList) {
	             				System.out.println(s.toString());
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static SensorInfo getSensor(Node node) {
		SensorInfo sinfo = new SensorInfo();
		if(node.getNodeType() == Node.ELEMENT_NODE) {
			Element element = (Element) node;
			sinfo.setName(getTagValue("name",element));
			sinfo.setId(getTagValue("id",element));
			sinfo.setIpAddress(getTagValue("ip-address",element));
		}
		return sinfo;
	}

	private static String getTagValue(String tag, Element element) {
		NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
		Node node = (Node) nodeList.item(0);
		return node.getNodeValue();
	}
}
