import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ServerInfoReader {
	public static void main(String[] args) {
		String filePath = "servers.xml";
		File xmlFile = new File(filePath);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);
			doc.getDocumentElement().normalize();
			System.out.println("Root Element: "+doc.getDocumentElement().getNodeName());
			NodeList nodeList = doc.getElementsByTagName("Server");
			// This loads the XML into the Memory
			List<ServerInfo> serverList = new ArrayList<ServerInfo>();
			System.out.println("Number of Nodes in XML File: "+nodeList.getLength());
			for (int i=0;i<nodeList.getLength();i++) {
				serverList.add(getServer(nodeList.item(i)));
			}
			PrintWriter pw = new PrintWriter(new File("serverinfo.txt"));
			for(ServerInfo s : serverList) {
				System.out.println(s.toString());
				pw.write(s.toString());
				pw.flush();
			}
			pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private static ServerInfo getServer(Node node) {
		ServerInfo sinfo = new ServerInfo();
		if(node.getNodeType() == Node.ELEMENT_NODE) {
			Element element = (Element) node;
			sinfo.setsName(getTagValue("name",element));
			sinfo.setPort(Integer.parseInt(getTagValue("port",element)));
			sinfo.setIpAddress(getTagValue("ip-address",element));
			sinfo.setPassword(getTagValue("password",element));
		}
		return sinfo;
		
	}
	private static String getTagValue(String tag, Element element) {
		NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
		Node node = (Node) nodeList.item(0);
		return node.getNodeValue();
	}
}
