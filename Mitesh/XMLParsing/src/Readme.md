# Server Bean Class

Server Bean Class contains the following information in the form of object parameters:

sName : Name of the Server

ipAddress : IP-Address of the Server

port : Port of the Server specified

For that we are taking the configuration as an XML File from the Deployer.

The format of the XML File is as follows:

	<Servers>

		<Server>	## This denotes an instance of the machine

			<name>[User Name of the Machine]</name>

			<ip-address>[IP-Address of the Machine]</ip-address>

			<port>[Port of the Machine]</port>

			<password>[Password of the Machine]</password>

		</Server>

	</Servers>


# Sensor Bean Class

Sensor Bean Class contains the following information in the form of object parameters:

name : Name of the Sensor

ipAddress : IP Address of the Sensor

id : ID of the Sensor

For that the configuration XML File has the following Format

	<Sensors>

		<Sensor>		## This denotes an instance of the sensor

			<name>[Name of the Sensor]</name>

			<ip-address>[IP Address of the Sensor]</ip-address>

			<id>[ID of the Sensor]</id>

		</Sensor>

	</Sensors>

# Service Information Bean Class

Service Info Bean Class contains the following information in the form of object parameters:

name : Name of the Service (Should be analogous to the python file name)

fileLoc : Location of the Python File

noParams : Number of Parameters required for the Service

For the format of the XML Configuration File is

	<Services>

		<Service>		## Denotes a services

			<name>[Name of the Service]</name>

			<location>[File Location of the Service]</location>

			<noParams>[Number of the Parameters]</#params>

		</Service>

	</Services>

The developer must provide each service in a seperate source python file and if the service has some dependencies like requirement of some modules than those modules must be installed on the application servers (if common PIP Packages) or included in the service source file.
