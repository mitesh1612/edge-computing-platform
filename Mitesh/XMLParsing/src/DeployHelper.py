import subprocess as sb
import csv
import time
f = open('serverinfo.txt','r')
servers = {}
for line in f:
	line = line.strip()
	vals = line.split(',')
	servers[vals[1]] = [vals[0],vals[2],vals[3]]
print "Servers I got: ",servers
for ip in servers:
    name,syspass,port = servers[ip]
    cmd = 'sshpass -p \"'+syspass+'\" ssh '+name+'@'+ip+' python ./mount_point/myinfo.py ./mount_point/Information.txt '+'"'+ip+'"'
    print "Command to Run: "+cmd
    sb.call(cmd,shell=True)
    # time.sleep(7)
    print "Command Ran!"


def make_comparator():
    def compare(x,y):
        if x[0] < y[0] :
            return -1
        elif y[0] < x[0]:
            return 1
        else:
            if x[1] < y[1]:
                return -1
            elif y[1] < x[1]:
                return 1
            else:
                return 0
    return compare
def createDict():
    array = []
    with open("Information.txt", "r") as ins:    
        for line in ins:
            text = line.rstrip('\n').split(',')
            array.append(text)
    return array
myTuples = createDict()      
tempList = sorted(myTuples,cmp=make_comparator(),reverse = True)
lBip = tempList.pop(0)[2]
loadBalancer = list(servers[lBip])
servers.pop(lBip)
loadBalancer.insert(0,lBip)
appServers = []
for key in servers:
    temp = []
    temp.append(key)
    for ele in servers[key]:
        temp.append(ele)
    appServers.append(temp)

print "App Servers: "+str(appServers)
print "Load Balancers: "+str(loadBalancer)
# In each element of App Servers List, elements are ip,uname,password,port
# cmd = 'zip app_logics.zip ./app_logics/*'
# sb.call(cmd,shell=True)
# for serverEle in appServers:
#     cmd = 'sshpass -p \"'+serverEle[2]+'\" scp app_logics.zip '+serverEle[1]+'@'+serverEle[0]+':/home/'+serverEle[1]+'/'
#     print "Copy Command: "+cmd
#     sb.call(cmd,shell=True)
#     print "Command Ran!"

# print "App Logics Delivered"