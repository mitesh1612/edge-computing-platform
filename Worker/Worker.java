import com.rabbitmq.client.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Worker {

  private static final String TASK_QUEUE_NAME = "lb_out_queue";
  private static ConnectionFactory connectionFactory, Factory;
  private static Connection task_connection, output_connection;
  private static Channel task_channel, output_channel;
  
  private static void initialize1() throws Exception
  {
	  connectionFactory = new ConnectionFactory();
//    InetAddress inetAddress = InetAddress.getLocalHost();
    
    connectionFactory.setUsername("guest");
    connectionFactory.setPassword("guest");
    connectionFactory.setVirtualHost("/");
   // connectionFactory.setHost(inetAddress.getHostAddress());
    connectionFactory.setPort(5672);
    connectionFactory.setHost("10.3.1.59");
    task_connection = connectionFactory.newConnection();
    task_channel = task_connection.createChannel();
    task_channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);
    
  }
  private static void initialize2(String qname) throws Exception
  {
    Factory = new ConnectionFactory();
    Factory.setUsername("guest");
    Factory.setPassword("guest");
    Factory.setVirtualHost("/");
   // connectionFactory.setHost(inetAddress.getHostAddress());
    Factory.setPort(5672);
    Factory.setHost(qname);
    output_connection = Factory.newConnection();
    output_channel = output_connection.createChannel();

    output_channel.queueDeclare(qname, true, false, false, null);
  }
  

  public static void main(String[] argv) throws Exception {

    initialize1();
    System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
    
//    channel.basicQos(2);

    final Consumer consumer = new DefaultConsumer(task_channel) {
      @Override
      public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
        
    	  String message1 = new String(body, "UTF-8");
        JSONObject newJObject = null;
        System.out.println(" [x] Received 1 : '" + message1 + "'");
        try {
        		
          doWork(message1,newJObject);  
        } catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
        	System.out.println(" [x] Done");
            task_channel.basicAck(envelope.getDeliveryTag(), false);
        }
      }
    };
    task_channel.basicConsume(TASK_QUEUE_NAME, false, consumer);
  }

  private static void doWork(String message1,JSONObject newJObject) throws Exception{
	  JSONParser parser = new JSONParser();
	  newJObject = (JSONObject) parser.parse(message1);
	  String qname = (String) newJObject.get("qname");
	 	 String servicename = (String) newJObject.get("servicename");
	 	 String type_param = (String) newJObject.get("type_param");
	 	 String param = (String) newJObject.get("param");
	 	 
	 	 System.out.println(qname);
	 	 System.out.println(servicename);

     String cmd = "python ";
     cmd += servicename;
     cmd += ".py ";
     cmd += param;
     System.out.println(cmd);
//	 	 Process p = Runtime.getRuntime().exec(cmd);
     ProcessBuilder pb = new ProcessBuilder("python",servicename+".py",""+param);
     Process p = pb.start(); 
     
	 	 
	 	BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
	 	 
	 	 
	 	System.out.println(" [x] Sent 1 : '" + param + "'");
	 	initialize2(qname);
	 	String output_string = "";
	 	while ((output_string = in.readLine()) != null) {
	 		
	 		System.out.println("output from logic =  "+output_string);
            output_channel.basicPublish("",qname,MessageProperties.PERSISTENT_TEXT_PLAIN,output_string.getBytes("UTF-8"));
        }
//	 	output_channel.basicPublish("",qname,MessageProperties.PERSISTENT_TEXT_PLAIN,output_string.getBytes("UTF-8"));
//	 	    System.out.println(" [x] Sent 2 : '" + param + "'");

	 	   output_channel.close();
	 	  output_connection.close();
  }
}
